import { gql } from "@apollo/client";

export const GET_YEARS = gql`
    query getYears {
        getYears
    }
`;

export const GET_PREVIEW = gql`
    query getPreview($year: String!) {
        getMagPreviewsByYear(year: $year) {
            date
            downloads
            views
            previewUrl
            issue
        }
    }
`;

export const GET_MAG = gql`
    query getMag($issue: String!) {
        getMagImgsUrls(issue: $issue)
    }
`;

export const INCREASE_VIEW_COUNT = gql`
    mutation addViews($year: String!, $issue: String!) {
        addViews(year: $year, issue: $issue)
    }
`;
