import styled from "styled-components";

export const ContentContainer = styled.main`
    overflow: hidden;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    padding: 1.5rem 2.5rem 2.5rem;

    @media (orientation: landscape) {
        @media screen and (min-width: 1024px) {
            padding: 1.5rem 4rem;
        }
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            padding: 1.5rem 5rem 3rem;
        }
    }
`;

export const FirefoxBugTempFix = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
`;

// will change in future if I come up with a better solution

export const ImgContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    @media (orientation: landscape) {
        width: 25%;

        @media screen and (min-width: 1024px) {
            width: 35%;
        }
        @media screen and (min-width: 1440px) {
            width: 25%;
        }
    }

    @media (orientation: portrait) {
        height: 60%;
    }
`;
