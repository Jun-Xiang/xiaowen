import React, { useEffect, useRef, useState, forwardRef } from "react";
import { useQuery } from "@apollo/client";
import { GET_PREVIEW } from "../../query";
import { gsap } from "gsap";
import { FirefoxBugTempFix, ContentContainer, ImgContainer } from "./ContentElements";
import Header from "../Header";
import SelectIssue from "../SelectIssue";
import SelectYear from "../SelectYear";
import LangSelect from "../LangSelect";
import Preview from "../Preview";
import Details from "../Details";
import SideInfo from "../SideInfo";
import Action from "../Action";
import Counter from "../Counter";

const Content = forwardRef(({ status, setStatus, setSelectedIssue, isEn, setIsEn }, contentRef) => {
    const headerRef = useRef();
    const counterRef = useRef();
    const actionRef = useRef();
    const sideInfoRef = useRef();
    const [counter, setCounter] = useState(1);
    const [yearsLoading, setYearsLoading] = useState(true);
    const [selectedYear, setSelectedYear] = useState();
    const [previousSelectedYear, setPreviousSelectedYear] = useState();
    const [imgsLoadStatus, setImgsLoadStatus] = useState(0);
    // Query for Mag informations
    const { loading, error, data } = useQuery(GET_PREVIEW, {
        variables: {
            year: selectedYear,
        },
        skip: !selectedYear,
    });
    // TODO: Show loading bar when read now button is pressed and move the read now text down (Future usage)
    // TODO: Change so that the select can contain a lot of years (Future usage)

    // Reset state useEffect
    useEffect(
        _ => {
            if (status === "CHANGE_YEAR") {
                // If year changed then only reset img load status
                if (imgsLoadStatus !== 0 && previousSelectedYear !== selectedYear) {
                    // Reset all related state
                    setImgsLoadStatus(0);
                    setPreviousSelectedYear(selectedYear);
                    setCounter(1);
                }
            }
        },
        [
            status,
            imgsLoadStatus,
            setImgsLoadStatus,
            selectedYear,
            previousSelectedYear,
            setPreviousSelectedYear,
            setCounter,
        ]
    );

    useEffect(
        _ => {
            let timeout;
            // if mag info, years and imgs finish loading
            if (data && !yearsLoading && imgsLoadStatus === data.getMagPreviewsByYear.length) {
                if (status === "LOADING" || status === "CHANGE_YEAR")
                    timeout = setTimeout(setStatus("LOADING_TRANSITION"), 1500);
            }

            return _ => clearTimeout(timeout);
        },
        [data, yearsLoading, setStatus, imgsLoadStatus, status]
    );

    useEffect(
        _ => {
            if (status === "READ_TRANSITION" && data) {
                setSelectedIssue({
                    issue: data.getMagPreviewsByYear[counter - 1].issue,
                    year: selectedYear,
                });
            }
        },
        [counter, setSelectedIssue, status, data, selectedYear]
    );

    // Animation useEffect
    useEffect(
        _ => {
            let topRefs;
            let botRefs;
            if (matchMedia("(orientation: landscape)").matches) {
                topRefs = [headerRef.current];
                botRefs = [counterRef.current, actionRef.current, sideInfoRef.current];
            } else if (matchMedia("(orientation: portrait)").matches) {
                topRefs = [headerRef.current, sideInfoRef.current];
                botRefs = [actionRef.current];
            }
            const topFromVars = {
                opacity: 0,
                yPercent: -100,
            };
            const botFromVars = {
                yPercent: 100,
                opacity: 0,
            };
            const toVars = { opacity: 1, yPercent: 0 };
            const options = {
                duration: 0.5,
                ease: "expo.out",
            };

            if (status === "LOADING" || status === "LOADING_TRANSITION") {
                gsap.set(contentRef.current, { opacity: 0 });
            } else if (status === "TRANSITION") {
                gsap.set(contentRef.current, { opacity: 1 });
                gsap.set([...topRefs, ...botRefs], { pointerEvents: "none" });
                gsap.fromTo(topRefs, topFromVars, { ...toVars, ...options });
                gsap.fromTo(botRefs, botFromVars, { ...toVars, ...options });
            } else if (status === "READ_TRANSITION") {
                // reversing animation from TRANSITION
                gsap.to(topRefs, { ...topFromVars, ...options });
                gsap.to(botRefs, { ...botFromVars, ...options });
            } else if (status === "DONE") {
                gsap.set([...topRefs, ...botRefs], { pointerEvents: "auto" });
            }
        },
        [status, contentRef]
    );

    const loadingConditions = loading || !selectedYear;
    if (error) return error;

    const { getMagPreviewsByYear } = data || {};
    const onLoadFn = _ => setImgsLoadStatus(s => s + 1);
    // loadImgs function
    // To preload and cache imgs so that images load smoothly
    const loadImgs = _ =>
        getMagPreviewsByYear &&
        getMagPreviewsByYear.map(({ previewUrl }, i) => (
            <img
                alt="img previews"
                style={{ display: "none" }}
                onLoad={onLoadFn}
                src={previewUrl}
                key={i}
            />
        ));
    return (
        <ContentContainer ref={contentRef}>
            <FirefoxBugTempFix>
                <Header ref={headerRef}>
                    <div id="selection" style={{ display: "flex", alignItems: "center" }}>
                        <LangSelect setIsEn={setIsEn} isEn={isEn} />
                        <SelectYear
                            setYearsLoading={setYearsLoading}
                            setSelectedYear={setSelectedYear}
                            setStatus={setStatus}
                        />
                    </div>
                </Header>
                {(status === "CHANGE_YEAR" || status === "LOADING") && loadImgs()}
                {!loadingConditions && (
                    <SelectIssue
                        isEn={isEn}
                        status={status}
                        issueLength={getMagPreviewsByYear.length}
                        setCounter={setCounter}
                        counter={counter}
                        setStatus={setStatus}
                    />
                )}

                {!loadingConditions && (
                    <ImgContainer>
                        <Preview
                            status={status}
                            setStatus={setStatus}
                            {...data}
                            counter={counter}
                        />
                        <Details
                            status={status}
                            setStatus={setStatus}
                            {...data}
                            counter={counter}
                            isEn={isEn}
                        />
                    </ImgContainer>
                )}
                {getMagPreviewsByYear && (
                    <Counter
                        ref={counterRef}
                        current={counter}
                        status={status}
                        total={getMagPreviewsByYear.length}
                    />
                )}
                <Action ref={actionRef} setStatus={setStatus} />
                <SideInfo ref={sideInfoRef} isEn={isEn} />
            </FirefoxBugTempFix>
        </ContentContainer>
    );
});

export default Content;
