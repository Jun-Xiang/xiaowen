import React, { forwardRef } from "react";
import { DialogContainer, Modal, Button, TitleWrapper, TextWrapper } from "./DialogComponents";
import Cross from "../Cross";

const Dialog = forwardRef(({ title, msg, btnText, btnClick, close, additionalStyles }, ref) => {
    return (
        <DialogContainer ref={ref} additionalStyles={additionalStyles}>
            <Modal>
                <TitleWrapper>
                    <p>{title}</p>
                    <Cross onClick={close} additionalStyles={"transform: translateX(1.1em);"} />
                </TitleWrapper>
                <TextWrapper>
                    <p>{msg}</p>
                </TextWrapper>
                <Button onClick={btnClick}>
                    <p>{btnText}</p>
                </Button>
            </Modal>
        </DialogContainer>
    );
});

export default Dialog;
