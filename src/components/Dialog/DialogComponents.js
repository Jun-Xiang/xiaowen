import styled from "styled-components";

export const DialogContainer = styled.div`
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    z-index: 1001;
    ${({ additionalStyles }) => additionalStyles}
`;

export const Modal = styled.div`
    border-radius: 3px;
    width: 80%;
    padding: 5% 0;
    background: whitesmoke;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    box-shadow: rgba(0, 0, 0, 0.1) 0 10px 50px;
    font-family: var(--fontMe);
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    @media screen and (min-width: 768px) {
        width: 60%;
    }

    @media screen and (min-width: 1024px) {
        width: 40%;
        padding: 3% 0;
    }

    @media screen and (min-width: 1280px) {
        width: 30%;
        padding: 2% 0;
    }
`;

export const TitleWrapper = styled.div`
    width: 80%;
    height: 15%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 10px;
    font-size: 1.2em;

    @media screen and (min-width: 768px) {
        margin-bottom: 20px;
        font-size: 1.5em;
    }

    p {
        font-family: var(--fontEb);
    }
`;

export const TextWrapper = styled.div`
    width: 80%;
    font-family: var(--fontM);
    opacity: 0.7;
    margin-bottom: 25px;
    line-height: 1.75;
    font-size: 0.875em;

    @media screen and (min-width: 768px) {
        margin-bottom: 55px;
        font-size: 1em;
    }
`;

export const Button = styled.div`
    width: 80%;
    height: 30px;
    font-family: var(--fontEb);
    background-color: #fed663;
    font-size: 0.875em;
    cursor: pointer;
    border-radius: 3px;
    transition: 0.25s;

    @media screen and (min-width: 768px) {
        height: 40px;
        font-size: 1em;
    }

    &:hover {
        background: #eec44b;
    }

    p {
        display: inline-block;
        position: relative;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
`;
