import React from "react";
import { LangContainer, LangSelect as Select } from "./LangElements";

const LangSelect = ({ setIsEn, isEn }) => {
    return (
        <LangContainer>
            <Select selected={isEn} onClick={_ => setIsEn(true)}>
                en
            </Select>
            <Select selected={!isEn} lang="cn" onClick={_ => setIsEn(false)}>
                华语
            </Select>
        </LangContainer>
    );
};

export default LangSelect;
