import styled from "styled-components";
// Language Selection
export const LangContainer = styled.div`
    margin-right: 1rem;
    cursor: pointer;

    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            margin-right: 2rem;
        }
        @media screen and (min-width: 1024px) {
            margin-right: 3rem;
        }
    }
`;

export const LangSelect = styled.p`
    width: max-content;
    text-decoration: underline;
    font-family: var(--fontSb);
    font-size: 0.875rem;
    font-size: 1rem;
    ${({ lang }) => lang === "cn" && "font-weight: bold;"};
    ${({ selected }) =>
        selected &&
        `
        display: none;
    `}
    @media (orientation: landscape) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }

        @media screen and (min-width: 1024px) {
            font-size: 1.5rem;
        }
    }
`;
