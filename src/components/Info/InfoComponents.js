import styled from "styled-components";

export const ShadowOverlay = styled.div`
    --divide: 2;
    --widthHeight: 15%;
    border-radius: 50%;
    width: var(--widthHeight);
    padding-top: var(--widthHeight);
    opacity: 0.6;
    box-shadow: 0 0 0 5000px rgba(0, 0, 0, 0.6);
    position: absolute;
    top: calc((100% - ${({ pageHeight }) => pageHeight}px) / 2);
    left: calc(50% + ${({ pageWidth }) => pageWidth}px / var(--divide));
    transform: translate(-65%, -40%);
    ${_ => (localStorage.getItem("showedShadow") ? "--widthHeight:0 !important;" : "")}
    z-index: 1000;

    @media screen and (min-width: 768px) {
        --widthHeight: 10%;
    }

    @media screen and (min-width: 1024px) {
        --widthHeight: 7%;
        --divide: 1.5;
        transform: translate(-10%, -40%);
    }

    @media screen and (min-width: 1280px) {
        --widthHeight: 5%;
        transform: translate(-10%, -30%);
    }
`;
