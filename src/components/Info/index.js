import React, { useLayoutEffect, useRef } from "react";
import gsap from "gsap";
import { ShadowOverlay } from "./InfoComponents";
import Dialog from "../Dialog";

const Info = ({ isEn, pageWidth, pageHeight, setOpenInfo, delay }) => {
    const overlayRef = useRef();
    const dialogRef = useRef();
    const close = _ => {
        delay.current = 0;
        if (!localStorage.getItem("showedShadow")) localStorage.setItem("showedShadow", true);
        setOpenInfo(false);
    };

    useLayoutEffect(
        _ => {
            const commonTween = {
                opacity: 0,
                duration: 0.5,
                delay: delay.current,
                ease: "expo.out",
            };
            gsap.from(overlayRef.current, commonTween);
            gsap.from(dialogRef.current, {
                y: "50%",
                ...commonTween,
            });
        },
        [delay]
    );

    return (
        <>
            <ShadowOverlay ref={overlayRef} pageWidth={pageWidth} pageHeight={pageHeight} />
            <Dialog
                ref={dialogRef}
                btnClick={close}
                close={close}
                title={isEn ? "Tutorial" : "讲解"}
                msg={
                    isEn
                        ? "Hover over to the top right or bottom right corner and drag to turn to next page. (Tapping the corners work too)"
                        : "将鼠标悬停在右上角或右下角，然后拖动以转到下一页。 （也可以点击角落）"
                }
                btnText={isEn ? "Okay, I got it." : "好，我知道了。"}
            />
        </>
    );
};

export default Info;
