import styled from "styled-components";

export const SideInfoContainer = styled.section`
    display: flex;
    justify-content: flex-end;
    position: absolute;
    right: 0;
    @media (orientation: landscape) {
        bottom: 1.5rem;
        align-items: flex-end;
    }
    @media (orientation: portrait) {
        top: 0;
        height: 10%;
        align-items: center;
    }

    a {
        margin-left: 1rem;
        display: flex;
        align-items: center;
        cursor: pointer;

        @media (orientation: portrait) {
            @media screen and (min-width: 1024px) {
                margin-left: 3rem;
            }
        }
    }

    img {
        height: 1.3rem;
        cursor: pointer;

        @media (orientation: portrait) {
            @media screen and (min-width: 1024px) {
                height: 2rem;
            }
        }
    }
`;
