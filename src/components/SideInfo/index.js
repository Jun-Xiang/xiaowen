import React, { useContext, forwardRef } from "react";
import { SideInfoContainer } from "./SideInfoElements";
import { DialogContext } from "../../App";
import info from "../../assets/img/info.svg";
import gitlab_icon from "../../assets/img/gitlab_icon.svg";

const SideInfo = forwardRef(({ isEn }, ref) => {
    const { setDialogMsg, setShowDialog } = useContext(DialogContext);

    const onClick = _ => {
        setDialogMsg({
            title: isEn ? "Info" : "信息",
            msg: isEn
                ? "This website is made by Chung Ling High School student, Ooi Jun Xiang."
                : "该网站是由锺灵中学学生,黄畯祥创建的.",
            btnText: isEn ? "OK" : "好的",
            btnClick: _ => setShowDialog(false),
            close: _ => setShowDialog(false),
        });
        setShowDialog(true);
    };

    return (
        <SideInfoContainer id="side-info" ref={ref}>
            <img src={info} alt="info icon" onClick={onClick} />
            <a
                href="https://gitlab.com/Jun-Xiang/xiaowen"
                rel="noopener noreferrer"
                target="_blank">
                <img src={gitlab_icon} alt="gitlab icon" />
            </a>
        </SideInfoContainer>
    );
});

export default SideInfo;
