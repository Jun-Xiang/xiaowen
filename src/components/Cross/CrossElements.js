import styled from "styled-components";

export const Line = styled.span`
    width: 0.8em;
    height: 2px;
    background-color: black;
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    ${({ lineStyles }) => lineStyles}
`;

export const CrossContainer = styled.div`
    width: 1.5em;
    height: 1.5em;
    cursor: pointer;
    position: relative;
    z-index: 10000;
    opacity: 0.6;
    transition: opacity 0.25s;
    ${({ additionalStyles }) => additionalStyles}

    span:nth-child(1) {
        transform: translate(-50%, -50%) rotate(45deg);
    }

    span:nth-child(2) {
        transform: translate(-50%, -50%) rotate(-45deg);
    }

    &:hover {
        opacity: 1;
    }
`;
