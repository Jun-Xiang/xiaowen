import React from "react";
import { CrossContainer, Line } from "./CrossElements";

const Cross = ({ additionalStyles, lineStyles, onClick }) => {
    return (
        <CrossContainer onClick={onClick} additionalStyles={additionalStyles}>
            <Line lineStyles={lineStyles} />
            <Line lineStyles={lineStyles} />
        </CrossContainer>
    );
};

export default Cross;
