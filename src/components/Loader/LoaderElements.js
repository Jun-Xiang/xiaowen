import styled, { keyframes } from "styled-components";
const spin = keyframes`
    100% {
        transform: rotate(360deg);
    }
`;

const dash = keyframes`
    0% {
        stroke-dasharray: 1 200;
        stroke-dashoffset: 0;
    }
    50% {
        stroke-dasharray: 98 200;
        /*push */
        stroke-dashoffset: -35px;
    }
    100% {
        stroke-dasharray: 98 200;
        stroke-dashoffset: -125px;
    }
`;

export const LoadingOverlay = styled.div`
    height: 100%;
    width: 100%;
    background-color: rgba(107, 129, 140, 0.2);
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    z-index: 100;
`;

export const Loader = styled.div`
    width: 100px;
    height: 25%;
`;

export const SVG = styled.svg.attrs(_ => ({ viewBox: "25,25,50,50" }))`
    width: 100%;
    height: 100%;
    transform-origin: 50% 50%;
    animation: ${spin} 2s linear infinite;
`;

export const Circle = styled.circle.attrs(_ => ({
    cx: "50",
    cy: "50",
    r: "20",
    fill: "none",
    strokeWidth: "2",
}))`
    stroke-dasharray: 1 200;
    stroke-dashoffset: 0;
    stroke-linecap: round;
    animation: ${dash} 1.5s ease-in-out infinite;
    stroke: rgb(107, 129, 140);
`;
