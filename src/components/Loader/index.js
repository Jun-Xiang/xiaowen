import React from "react";
import { Circle, Loader as Box, LoadingOverlay, SVG } from "./LoaderElements";

function Loader() {
    return (
        <LoadingOverlay>
            <Box>
                <SVG>
                    <Circle />
                </SVG>
            </Box>
        </LoadingOverlay>
    );
}

export default Loader;
