import React from "react";
import { NumberContainer, Current, Total } from "./PageNumberElement";

const PageNumber = ({ total, current }) => {
    return (
        <NumberContainer>
            <Current>{current}</Current>/<Total>{total}</Total>
        </NumberContainer>
    );
};

export default PageNumber;
