import styled from "styled-components";

export const NumberContainer = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 25%;
    position: absolute;
    top: 5%;

    @media screen and (min-width: 768px) {
        width: 10%;
    }
`;

export const Current = styled.span``;

export const Total = styled.span``;
