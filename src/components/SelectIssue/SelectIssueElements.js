import styled, { css } from "styled-components";

export const IssueContainer = styled.section`
    @media (orientation: landscape) {
        width: 80%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

        @media screen and (min-width: 1280px) {
            width: 70%;
        }
    }
    @media (orientation: portrait) {
        position: absolute;
        top: 87%;
        left: 50%;
        transform: translate(-50%, 0);
        @media screen and (min-width: 768px) {
        }
        @media screen and (min-width: 1280px) {
        }
    }
`;

export const IssueSelection = styled.div`
    @media (orientation: landscape) {
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
    }
    @media (orientation: portrait) {
        display: none;
    }
`;

const sameStyles = css`
    --radius: 5vw;
    border-radius: 50%;
    width: var(--radius);
    height: var(--radius);
    border: 1px solid #40424f;
    cursor: pointer;
    will-change: transform, background-color;
    transition: all 0.25s ease;

    @media screen and (min-width: 1280px) {
        --radius: 4vw;
    }

    img,
    svg {
        position: relative;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height: 1em;
        fill: #40424f;
    }

    &:hover {
        transform: scale(1.1);
        background-color: #40424f;
        svg {
            fill: white;
        }
    }
`;

export const Previous = styled.div`
    ${sameStyles}
`;

export const Next = styled.div`
    ${sameStyles}
`;

export const TinyCircleContainer = styled.div`
    display: flex;
    @media (orientation: landscape) {
        display: none;
    }
`;

export const TinyCircle = styled.div`
    --radius: 0.4rem;
    width: var(--radius);
    height: var(--radius);
    border-radius: 50%;
    border: 1px solid #40424f;
    transition: background 0.5s ease-in-out;

    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            --radius: 0.5rem;
        }

        @media screen and (min-width: 1024px) {
            --radius: 0.7rem;
        }
    }

    &:not(:nth-child(1)) {
        margin-left: 1rem;

        @media (orientation: portrait) {
            @media screen and (min-width: 768px) {
                margin-left: 2rem;
            }

            @media screen and (min-width: 1024px) {
                margin-left: 3rem;
            }
        }
    }

    &.selected {
        background: #40424f;
    }
`;
