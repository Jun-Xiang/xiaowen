import React, { useEffect, useRef, useCallback } from "react";
import { gsap } from "gsap";
import {
    IssueContainer,
    IssueSelection,
    Next,
    Previous,
    TinyCircle,
    TinyCircleContainer,
} from "./SelectIssueElements";

const SelectIssue = ({ status, setStatus, issueLength, counter, setCounter, isEn }) => {
    const containerRef = useRef();
    const touchstartRef = useRef();

    const changeCounter = useCallback(
        (cb, i = null) => {
            if (counter !== i) {
                setStatus("CHANGE_COUNTER");
                setTimeout(_ => cb(i), 500);
                setTimeout(_ => setStatus("DONE"), 1000);
            }
        },
        [counter, setStatus]
    );

    const increaseCb = useCallback(
        _ =>
            setCounter(c => {
                if (c + 1 > issueLength) {
                    return 1;
                }
                return c + 1;
            }),
        [issueLength, setCounter]
    );

    const decreaseCb = useCallback(
        _ =>
            setCounter(c => {
                if (c - 1 <= 0) {
                    return issueLength;
                }
                return c - 1;
            }),
        [issueLength, setCounter]
    );

    const selectCb = i => setCounter(i);

    const increase = useCallback(_ => changeCounter(increaseCb), [changeCounter, increaseCb]);
    const decrease = useCallback(_ => changeCounter(decreaseCb), [changeCounter, decreaseCb]);
    const selectCounter = i => changeCounter(selectCb, i);

    useEffect(
        _ => {
            const touchstartFn = e => {
                if (e.target.parentNode.id !== "preview" || status !== "DONE") return;
                touchstartRef.current = e.touches[0].clientX;
            };
            const touchendFn = e => {
                if (e.target.parentNode.id !== "preview" || status !== "DONE") return;
                const touchEnd = e.changedTouches[0].clientX;
                if (touchstartRef.current - touchEnd > 20) increase();
                else if (touchstartRef.current - touchEnd <= -20) decrease();
            };
            window.addEventListener("touchstart", touchstartFn);
            window.addEventListener("touchend", touchendFn);

            return _ => {
                window.removeEventListener("touchstart", touchstartFn);
                window.removeEventListener("touchend", touchendFn);
            };
        },
        [increase, decrease, status]
    );

    useEffect(
        _ => {
            const containerOptions = {
                duration: 0.5,
                delay: 0.25,
                ease: "expo.out",
            };

            if (status === "TRANSITION") {
                gsap.set(containerRef.current, { pointerEvents: "none" });
                gsap.fromTo(
                    containerRef.current,
                    { opacity: 0 },
                    { opacity: 1, ...containerOptions }
                );
            } else if (status === "READ_TRANSITION") {
                gsap.set(containerRef.current, { pointerEvents: "none" });
                gsap.to(containerRef.current, { ...containerOptions, opacity: 0, delay: 0 });
            } else if (status === "CHANGE_COUNTER") {
                gsap.set(containerRef.current, { pointerEvents: "none" });
            } else if (status === "DONE") {
                gsap.set(containerRef.current, { pointerEvents: "auto" });
            }
        },
        [status]
    );

    return (
        <IssueContainer id="select-issue" ref={containerRef}>
            <IssueSelection>
                <Previous onClick={decrease}>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="52.893"
                        height="100.067"
                        viewBox="0 0 52.893 100.067">
                        <path
                            d="M24.42,99.16a2.84,2.84,0,0,0,2,.84,2.74,2.74,0,0,0,2-.84L75.58,52a2.81,2.81,0,0,0,0-4L28.42.83a2.83,2.83,0,1,0-4,4L69.58,50,24.42,95.16a2.82,2.82,0,0,0,0,4Z"
                            transform="translate(76.416 100) rotate(-180)"
                        />
                    </svg>
                </Previous>
                <Next onClick={increase}>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="52.893"
                        height="100.067"
                        viewBox="0 0 52.893 100.067">
                        <path
                            d="M24.42,99.16a2.84,2.84,0,0,0,2,.84,2.74,2.74,0,0,0,2-.84L75.58,52a2.81,2.81,0,0,0,0-4L28.42.83a2.83,2.83,0,1,0-4,4L69.58,50,24.42,95.16a2.82,2.82,0,0,0,0,4Z"
                            transform="translate(-23.523 0.067)"
                        />
                    </svg>
                </Next>
            </IssueSelection>
            <TinyCircleContainer>
                {[...Array(issueLength)].map((_, i) => {
                    return (
                        <TinyCircle
                            key={i}
                            onClick={_ => selectCounter(i + 1)}
                            className={i + 1 === counter ? "selected" : ""}></TinyCircle>
                    );
                })}
            </TinyCircleContainer>
        </IssueContainer>
    );
};

export default SelectIssue;
