import React, { forwardRef, useEffect, useRef } from "react";
import gsap from "gsap";
import { CounterContainer as Container, Current, Total } from "./CounterElements";

const Counter = forwardRef(({ status, current, total }, ref) => {
    const currentRef = useRef();
    useEffect(
        _ => {
            if (status === "CHANGE_COUNTER") {
                const tween = gsap.to(currentRef.current, {
                    yPercent: 100,
                    duration: 0.5,
                    ease: "expo.out",
                    onComplete: _ => tween.reverse(),
                });
            }
        },
        [status]
    );

    return (
        <Container ref={ref}>
            <Current ref={currentRef}>0{current}&nbsp;</Current>
            <Total>/&nbsp;0{total}</Total>
        </Container>
    );
});

export default Counter;
