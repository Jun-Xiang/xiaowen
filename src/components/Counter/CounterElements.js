import styled from "styled-components";

export const CounterContainer = styled.div`
    --fontSize: 1.8rem;
    display: flex;
    justify-content: center;
    align-items: flex-end;
    position: absolute;
    bottom: 1.5rem;
    left: 0;
    overflow: hidden;

    @media (orientation: portrait) {
        display: none;
    }
`;

export const Current = styled.p`
    font-family: var(--fontBo);
    font-size: var(--fontSize);
`;

export const Total = styled.p`
    font-family: var(--fontM);
    font-size: calc(var(--fontSize) / 2);
`;
