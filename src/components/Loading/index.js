import React, { useEffect, useRef } from "react";
import { gsap } from "gsap";
import { LoadingContainer, LoadingImg } from "./LoadingElements";

import logo from "../../assets/img/clhs-logo.svg";

const Loading = ({ status, setStatus }) => {
    const loadingRef = useRef();
    const logoRef = useRef();

    useEffect(
        _ => {
            // Loading page element animation
            if (status === "LOADING_TRANSITION") {
                gsap.fromTo(
                    loadingRef.current,
                    {
                        y: 0,
                    },
                    {
                        y: "100%",
                        duration: 1,
                        ease: "expo.in",
                        onComplete: _ => setStatus("TRANSITION"),
                    }
                );
            } else if (status === "CHANGE_YEAR") {
                gsap.from(loadingRef.current, { y: "-100%", duration: 0.5, ease: "expo.out" });
            }
            // Logo Animation
            const commonLogoTween = {
                opacity: 0,
                duration: 1,
                ease: "expo.out",
            };
            if (status === "LOADING" || status === "CHANGE_YEAR") {
                gsap.from(logoRef.current, commonLogoTween);
            } else if (status === "LOADING_TRANSITION") {
                gsap.to(logoRef.current, {
                    ...commonLogoTween,
                    y: "-100%",
                });
            }
        },
        [status, setStatus]
    );

    return (
        <LoadingContainer ref={loadingRef}>
            <LoadingImg ref={logoRef} src={logo} alt="clhs logo" />
        </LoadingContainer>
    );
};

export default Loading;
