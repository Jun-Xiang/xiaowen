import styled from "styled-components";

export const LoadingContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    z-index: 10;
    background-color: #40424f;
`;

export const LoadingImg = styled.img`
    width: 30vw;

    @media screen and (min-width: 768px) {
        width: 25vw;
    }

    @media screen and (min-width: 1024px) {
        width: 15vw;
    }
    @media screen and (min-width: 1280px) {
        width: 10vw;
    }
`;
