import React, { useEffect, useRef, useContext } from "react";
import { gsap } from "gsap";
import { PreviewContainer, PreviewImg } from "./PreviewElements";
import { DialogContext } from "../../App";

const Preview = ({ status, setStatus, getMagPreviewsByYear: mags, counter }) => {
    const imgRef = useRef();
    const containerRef = useRef();
    const { setDialogMsg, setShowDialog } = useContext(DialogContext);

    useEffect(
        _ => {
            const fromVars = {
                opacity: 0,
                filter: "blur(1em)",
            };
            const toVars = {
                opacity: 1,
                filter: "blur(0px)",
            };
            const options = {
                duration: 0.5,
                ease: "expo.out",
            };

            if (status === "TRANSITION") {
                gsap.set([containerRef.current, containerRef.current.parentNode], {
                    clearProps: "all",
                });
                gsap.fromTo(imgRef.current, fromVars, {
                    ...toVars,
                    ...options,
                    delay: matchMedia("(min-width: 1024px)").matches ? 0.5 : 0.5,
                });
            } else if (status === "CHANGE_COUNTER") {
                const tween = gsap.to(imgRef.current, {
                    ...fromVars,
                    ...options,
                    onComplete: _ => tween.reverse(),
                });
            } else if (status === "READ_TRANSITION") {
                const oriMagHeight = 3526;
                const oriMagWidth = 2465;
                const magHeightToWidthRatio = oriMagHeight / oriMagWidth;
                const magWidthToHeightRatio = oriMagWidth / oriMagHeight;
                const options = {
                    duration: 0.5,
                    delay: 0.5,
                    onComplete: _ => setStatus("READ_LOADING"),
                };

                if (matchMedia("(orientation: landscape)").matches) {
                    const height = window.innerHeight * 0.8;
                    const width = height * magWidthToHeightRatio;
                    gsap.to(containerRef.current, {
                        height,
                        width,
                        marginLeft: width / 2,
                        ...options,
                    });
                } else if (matchMedia("(orientation: portrait)").matches) {
                    const width = window.innerWidth * 0.8;
                    const height = width * magHeightToWidthRatio;
                    gsap.to([containerRef.current, containerRef.current.parentNode], {
                        width,
                        height,
                        ...options,
                    });
                }
            } else if (status === "READ") {
                gsap.to(containerRef.current, {
                    opacity: 0,
                    duration: 0.5,
                    delay: 0.5,
                    ease: "expo.in",
                });
            }
        },
        [status, setStatus]
    );

    return (
        <PreviewContainer ref={containerRef} id="preview">
            <PreviewImg
                ref={imgRef}
                src={mags[counter - 1].previewUrl}
                onError={_ => {
                    setDialogMsg({
                        title: "Error",
                        msg: "Report to us",
                        btnText: "Report",
                        btnClick: _ => window.open("https://forms.gle/LCmG3ZC21XSqhmKz8", "_blank"),
                        close: _ => setShowDialog(false),
                    });
                    setShowDialog(true);
                }}
            />
        </PreviewContainer>
    );
};

export default Preview;
