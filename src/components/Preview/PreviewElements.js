import styled from "styled-components";

export const PreviewContainer = styled.section`
    @media (orientation: landscape) {
        width: 100%;
    }
    @media (orientation: portrait) {
        height: 100%;
    }
`;

export const PreviewImg = styled.img`
    /* Future fn only uncomment below */
    /* cursor: pointer; */
    display: flex;
    transition: margin 1s;

    @media (orientation: landscape) {
        width: 100%;
    }
    @media (orientation: portrait) {
        height: 100%;
    }
`;
