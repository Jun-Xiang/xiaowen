import React, { forwardRef } from "react";
import { ActionContainer, ReadNowBtn } from "./ActionElements";

import read_icon from "../../assets/img/read.svg";

const Action = forwardRef(({ setStatus }, ref) => {
    return (
        <ActionContainer id="action" ref={ref}>
            <ReadNowBtn onClick={_ => setStatus("READ_TRANSITION")}>
                <img src={read_icon} alt="Read icon" />
            </ReadNowBtn>
        </ActionContainer>
    );
});

export default Action;
