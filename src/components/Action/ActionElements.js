import styled, { css } from "styled-components";

const centerElement = css`
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

export const ActionContainer = styled.section`
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    position: absolute;
    top: 92%;
    left: 50%;
    transform: translate(-50%, 0);
    cursor: pointer;

    @media (orientation: landscape) {
        top: 90%;
    }
`;

const Button = styled.div`
    --radius: 4rem;
    width: var(--radius);
    height: var(--radius);
    border-radius: 50%;

    img {
        ${centerElement}
        width: 40%;
    }

    @media (orientation: landscape) {
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            --radius: 5rem;
        }

        @media screen and (min-width: 1024px) {
            --radius: 6.5rem;
        }
    }
`;

export const ReadNowBtn = styled(Button)`
    background: #fed663;
    position: relative;
    transition: background 0.25s ease-in-out;

    &:hover {
        background: #eec44b;
    }
`;

//  Not doing download feature
// export const DownloadBtn = styled(Button)`
//     border: 2px solid #e9c251;
//     transition: border 0.25s ease-in-out, background 0.25s ease-in-out;

//     &:hover {
//         background: #fed663;
//         border: 2px solid #fed663;
//     }
// `;

// export const ButtonText = styled.p`
//     ${centerElement}
//     display: inline-block;
//     font-family: var(--fontEb);
//     ${({ cn }) => cn && "font-weight: bold;"}
// `;

// export const IconBtn = styled.div`
//     width: 8vh;
//     height: 100%;
//     background: #ffebb3;
//     cursor: pointer;

//     @media screen and (min-width: 768px) {
//         display: none;
//     }

//     img {
//         ${centerElement}
//     }
// `;
