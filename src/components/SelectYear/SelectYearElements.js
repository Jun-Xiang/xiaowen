import styled from "styled-components";
import arrow_down from "../../assets/img/triangle.svg";

export const SelectContainer = styled.section`
    font-size: 0.875rem;

    @media (orientation: landscape) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }

        @media screen and (min-width: 1024px) {
            font-size: 1.5rem;
        }
    }
`;

export const YearSelect = styled.select`
    cursor: pointer;
    border: none;
    background: transparent;
    background-image: url(${arrow_down});
    background-repeat: no-repeat;
    background-position: 90% 50%;
    font-family: var(--fontBo);
    appearance: none;
    padding-right: 1em;
    text-decoration: underline;
`;

export const YearOption = styled.option`
    text-align: center;
`;
