import React, { forwardRef, useEffect } from "react";
import { useQuery } from "@apollo/client";
import { GET_YEARS } from "../../query";
import { SelectContainer, YearOption, YearSelect } from "./SelectYearElements";

const SelectYear = forwardRef(({ setSelectedYear, setStatus, setYearsLoading }, ref) => {
    const { loading, data, error } = useQuery(GET_YEARS, {
        onCompleted: data => setSelectedYear(data.getYears[0]),
    });

    useEffect(
        _ => {
            if (data) setYearsLoading(false);
        },
        [data, setYearsLoading]
    );

    if (loading) return null;
    if (error) return error;

    return (
        <SelectContainer id="select-year" ref={ref}>
            <YearSelect
                onChange={e => {
                    setSelectedYear(e.target.value);
                    setStatus("CHANGE_YEAR");
                }}>
                {data.getYears.map((year, i) => (
                    <YearOption value={year} key={i}>
                        {year}
                    </YearOption>
                ))}
            </YearSelect>
        </SelectContainer>
    );
});

export default SelectYear;
