import styled from "styled-components";

//  Header
export const HeaderContainer = styled.section`
    display: flex;
    position: relative;
    z-index: 5;
    height: 10%;
    justify-content: space-between;
    align-items: center;
    @media (orientation: landscape) {
    }
    @media (orientation: portrait) {
        width: 50%;
        div#selection {
            transform: translateX(50%);
        }
    }
`;

// Logo

export const LogoContainer = styled.div`
    position: relative;
    z-index: 3;
`;

export const LogoImg = styled.img`
    height: 3rem;

    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            height: 4rem;
        }
        @media screen and (min-width: 1024px) {
            height: 5rem;
        }
    }
`;
