import React, { forwardRef } from "react";
import { HeaderContainer, LogoContainer, LogoImg } from "./HeaderElements";

import logo from "../../assets/img/clhs-logo.svg";

const Header = forwardRef(({ children }, ref) => {
    return (
        <HeaderContainer ref={ref} id="header">
            <LogoContainer id="logo">
                <LogoImg src={logo} alt="clhs logo" />
            </LogoContainer>

            {children}
        </HeaderContainer>
    );
});

export default Header;
