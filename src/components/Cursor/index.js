import React, { useState, useEffect } from "react";
import { Cursor as C } from "./CursorElements";

const Cursor = () => {
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [hidden, setHidden] = useState(true);
    const [clicked, setClicked] = useState(false);
    const renderCursorCondition =
        matchMedia("(pointer:fine)").matches || !/Android|Mobi/i.test(navigator.userAgent);

    useEffect(_ => {
        const updatePosition = e => {
            setPosition({ x: e.clientX, y: e.clientY });
            setHidden(false);
        };
        const removeCursor = _ => setHidden(true);
        const onMouseDown = _ => setClicked(true);
        const onMouseUp = _ => setClicked(false);

        document.addEventListener("mousemove", updatePosition);
        document.addEventListener("mousedown", onMouseDown);
        document.addEventListener("mouseup", onMouseUp);
        document.body.addEventListener("mouseenter", updatePosition);
        document.body.addEventListener("mouseleave", removeCursor);
        return _ => {
            document.removeEventListener("mousemove", updatePosition);
            document.removeEventListener("mousedown", onMouseDown);
            document.removeEventListener("mouseup", onMouseUp);
            document.body.removeEventListener("mouseenter", updatePosition);
            document.body.removeEventListener("mouseleave", removeCursor);
        };
    });

    return (
        renderCursorCondition && (
            <C
                className={`${hidden ? "hidden" : ""} ${clicked ? "clicked" : ""}`}
                style={{ left: `${position.x}px`, top: `${position.y}px` }}
            />
        )
    );
};

export default Cursor;
