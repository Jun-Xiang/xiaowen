import styled from "styled-components";

export const Cursor = styled.div`
    width: 2em;
    height: 2em;
    border-radius: 50%;
    border: 2px solid black;
    transform: translate(-50%, -50%);
    position: fixed;
    z-index: 999999;
    pointer-events: none;
    transition: all 50ms ease;
    will-change: top, left;
    mix-blend-mode: difference;
    backface-visibility: hidden;

    &.hidden {
        opacity: 0;
    }

    &.clicked {
        transform: translate(-50%, -50%) scale(0.9);
        background-color: black;
    }
`;
