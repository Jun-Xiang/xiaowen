import React, { useEffect, useRef } from "react";
import { gsap } from "gsap";
import {
    DateUploaded,
    DetailsContainer,
    Downloads,
    MetadataContainer,
    Views,
} from "./DetailsElements";

const Details = ({ status, setStatus, getMagPreviewsByYear: mags, counter, isEn }) => {
    const dateRef = useRef();
    const dataRef = useRef();

    useEffect(
        _ => {
            const detailsData = [dateRef.current, dataRef.current];
            const options = {
                duration: 0.5,
                ease: "expo.out",
            };

            if (status === "TRANSITION") {
                gsap.fromTo(
                    detailsData,
                    { yPercent: -100 },
                    {
                        yPercent: 0,
                        stagger: 0.1,
                        ...options,
                        delay: 0.5,
                        onComplete: _ => setStatus("DONE"),
                    }
                );
            } else if (status === "READ_TRANSITION") {
                gsap.to(detailsData, { yPercent: -100, duration: 0.5 });
            } else if (status === "CHANGE_COUNTER") {
                const dataTween = gsap.to(detailsData, {
                    yPercent: -100,
                    ...options,
                    onComplete: _ => dataTween.reverse(),
                });
            }
        },
        [status, setStatus]
    );

    const { date, downloads, views } = mags[counter - 1];

    return (
        <DetailsContainer id="details">
            <DateUploaded ref={dateRef}>{date}</DateUploaded>
            <MetadataContainer ref={dataRef}>
                <Views>
                    {views} {isEn ? " views" : " 观看"}
                </Views>
                &sdot;
                <Downloads>
                    {downloads}
                    {isEn ? " downloads" : " 下载"}
                </Downloads>
            </MetadataContainer>
        </DetailsContainer>
    );
};

export default Details;
