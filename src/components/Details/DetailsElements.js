import styled from "styled-components";

// Outer container
export const DetailsContainer = styled.section`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    position: absolute;
    bottom: -1.25em;
    opacity: 0.8;
    overflow: hidden;
`;

// Metadata Container
export const MetadataContainer = styled.div`
    --padding: 0.3rem;
    display: flex;
    overflow: hidden;
    width: max-content;
    font-size: 0.75rem;
    font-family: var(--fontM);
    @media (orientation: landscape) {
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }
    }
`;

export const DateUploaded = styled.p`
    font-size: 0.75rem;
    @media (orientation: landscape) {
    }
    @media (orientation: portrait) {
        @media screen and (min-width: 768px) {
            font-size: 1rem;
        }
    }
`;

export const Views = styled.p`
    padding-right: var(--padding);
`;

export const Downloads = styled.p`
    padding-left: var(--padding);
`;
