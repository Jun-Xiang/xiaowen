import React, { useEffect, useState, useRef } from "react";
import gsap from "gsap";
import { useQuery, useMutation } from "@apollo/client";
import { GET_MAG, INCREASE_VIEW_COUNT } from "../../query";
import $ from "jquery";
import "@ksedline/turnjs";
import {
    MagContainer as Container,
    Magazine,
    ActionsContainer,
    IconContainer,
} from "./MagElements";
import Info from "../Info";
import Cross from "../Cross";
import PageNumber from "../PageNumber";
import info from "../../assets/img/info.svg";

const Mag = ({ selectedIssue, setStatus, status, isEn }) => {
    const containerRef = useRef();
    const delay = useRef(0.5);
    const [imgsLoadState, setImgsLoadState] = useState([]);
    const [magSize, setMagSize] = useState({});
    const [openInfo, setOpenInfo] = useState(false);
    const [pageNum, setPageNum] = useState(1);
    const { loading, error, data } = useQuery(GET_MAG, {
        variables: {
            issue: selectedIssue.issue,
        },
    });
    const [addViews] = useMutation(INCREASE_VIEW_COUNT, {
        variables: {
            year: selectedIssue.year,
            issue: selectedIssue.issue,
        },
    });

    useEffect(
        _ => {
            if (data) {
                const urls = data.getMagImgsUrls;
                setImgsLoadState(Array(urls.length).fill(false));
            }
        },
        [data, setImgsLoadState]
    );

    useEffect(
        _ => {
            const renderMagView = _ => {
                const oriMagHeight = 3526;
                const oriMagWidth = 2465;
                const magHeightToWidthRatio = oriMagHeight / oriMagWidth;
                const magWidthToHeightRatio = oriMagWidth / oriMagHeight;
                let width;
                let height;
                let percentage = 0.8;
                if (matchMedia("(orientation: landscape)").matches) {
                    height = window.innerHeight * percentage;
                    width = height * magWidthToHeightRatio;
                    // To reduce size until it fits
                    while (width * 2 > window.innerWidth) {
                        percentage -= 0.01;
                        height = window.innerHeight * percentage;
                        width = height * magWidthToHeightRatio;
                    }
                    $("#mag").turn({
                        height,
                        width: width * 2,
                        autoCenter: true,
                        acceleration: true,
                    });
                } else if (matchMedia("(orientation: portrait)").matches) {
                    width = window.innerWidth * percentage;
                    height = width * magHeightToWidthRatio;
                    // To reduce size until it fits
                    while (height > window.innerHeight * 0.8) {
                        percentage -= 0.01;
                        width = window.innerWidth * percentage;
                        height = width * magHeightToWidthRatio;
                    }
                    $("#mag").turn({
                        height,
                        width,
                        autoCenter: true,
                        acceleration: true,
                        display: "single",
                    });
                }
                setMagSize({
                    width,
                    height,
                });
                // Old version of jquery (still works as this is what the library required)
                // !Unable to do zoom function
                $("#mag").bind("turned", (e, page, view) => {
                    // remove 0 from array
                    let v = view.filter(Boolean);
                    setPageNum(v.join("-"));
                    // TODO: create a component to display page
                });
            };

            if (imgsLoadState.every(b => b) && imgsLoadState.length !== 0) {
                renderMagView();
                setStatus("READ");
            }
        },
        [imgsLoadState, setStatus]
    );

    useEffect(_ => {
        if (!localStorage.getItem("infoShown")) {
            setOpenInfo(true);
            localStorage.setItem("infoShown", true);
        }
    }, []);

    useEffect(
        _ => {
            if (!localStorage.getItem("addedViews")) {
                addViews();
                localStorage.setItem("addedViews", true);
            }
        },
        [addViews]
    );

    useEffect(
        _ => {
            if (status === "READ") {
                gsap.set(containerRef.current, {
                    opacity: 1,
                    delay: 0.5,
                    ease: "expo.out",
                });
            }
        },
        [status]
    );

    if (loading) return null;
    if (error) return `ERROR: ${error}`;
    const urls = data.getMagImgsUrls;
    const imgOnLoad = i => setImgsLoadState(s => s.map((b, index) => (index === i ? true : b)));
    const imgs = urls.map((url, i) => (
        <div key={i}>
            <img onLoad={_ => imgOnLoad(i)} src={url} alt="magazine" />
        </div>
    ));
    const closeMag = _ => {
        localStorage.removeItem("addedViews");
        gsap.set(containerRef.current, { pointerEvents: "none" });
        gsap.to(containerRef.current, {
            opacity: 0,
            duration: 0.5,
            onComplete: _ => setStatus("TRANSITION"),
        });
    };

    return (
        <Container id="viewport" ref={containerRef}>
            <ActionsContainer>
                <IconContainer onClick={_ => setOpenInfo(true)}>
                    <img src={info} alt="Info icon" />
                </IconContainer>
                <Cross
                    lineStyles={matchMedia("(min-width: 768px) ").matches && "width:1.5em;"}
                    onClick={closeMag}
                />
            </ActionsContainer>
            <PageNumber current={pageNum} total={imgsLoadState.length} />
            {openInfo && status === "READ" && (
                <Info
                    setOpenInfo={setOpenInfo}
                    isEn={isEn}
                    pageWidth={magSize.width}
                    pageHeight={magSize.height}
                    delay={delay}
                />
            )}
            <Magazine id="mag">{imgs}</Magazine>
        </Container>
    );
};

export default Mag;
