import React, { useEffect, useState, useRef } from "react";
import { useLazyQuery } from "@apollo/client";
import { GET_MAG } from "../query/index";
import $ from "jquery";
import "turn.js";

const Mag = ({ issue, isReadNowPressed, setIsMagLoading }) => {
    const [areImgsLoading, setAreImgsLoading] = useState([]);
    const touchstartRef = useRef();
    const [loadMagUrl, { loading, error, called, data }] = useLazyQuery(GET_MAG, {
        variables: {
            issue,
        },
    });

    useEffect(() => {
        if (isReadNowPressed) {
            loadMagUrl();
        }
    }, [isReadNowPressed, loadMagUrl]);

    useEffect(() => {
        if (data) {
            const [...urls] = data.getMagImgsUrls;
            setAreImgsLoading(Array(urls.length).fill(true));
        }
    }, [data, setAreImgsLoading]);

    useEffect(() => {
        const renderMagView = _ => {
            let clientWidth = document.body.clientWidth;
            const clientHeight = document.body.clientHeight;
            const width = clientHeight * (2465 / 3526);
            let height = ((clientWidth * 9) / 10) * (3526 / 2465);
            if (clientWidth > clientHeight)
                $(".mag-container").turn({
                    height: clientHeight,
                    width: width * 2,
                    autoCenter: true,
                    elevation: 50,
                    display: "double",
                    acceleration: true,
                    gradients: true,
                });
            else if (clientHeight > clientWidth) {
                if (clientHeight - height <= 100) {
                    clientWidth = (clientWidth * 7) / 10;
                } else {
                    clientWidth = (clientWidth * 9) / 10;
                }
                height = clientWidth * (3526 / 2465);
                $(".mag-container").turn({
                    height,
                    width: clientWidth,
                    autoCenter: true,
                    elevation: 50,
                    display: "single",
                    acceleration: true,
                    gradients: true,
                });
            }
        };

        if (!areImgsLoading.includes(true) && areImgsLoading.length !== 0) {
            setIsMagLoading(false);
            renderMagView();
            const handleTouchStart = e => {
                if (e.target.attributes.alt && e.target.attributes.alt === "mag") return;
                touchstartRef.current = e.touches[0].clientX;
            };
            const handleTouchEnd = e => {
                if (e.target.attributes.alt && e.target.attributes.alt === "mag") return;
                const touchEndX = e.changedTouches[0].clientX;
                // if is from right to left
                if (touchstartRef.current - touchEndX >= 20) $(".mag-container").turn("next");
                else if (touchstartRef.current - touchEndX <= -10)
                    $(".mag-container").turn("previous");
            };

            window.addEventListener("touchstart", handleTouchStart);
            window.addEventListener("touchend", handleTouchEnd);

            return _ => {
                window.removeEventListener("touchstart", handleTouchStart);
                window.removeEventListener("touchend", handleTouchEnd);
            };
        }
    }, [areImgsLoading, setIsMagLoading]);

    if (loading || !called) return null;
    if (error) return `Error! ${error}`;
    const [...urls] = data.getMagImgsUrls;
    const imgs = urls.map((url, i) => {
        const handleOnLoad = _ =>
            setAreImgsLoading(prevState => [
                ...prevState.slice(0, i),
                false,
                ...prevState.slice(i + 1, prevState.length),
            ]);
        return (
            <div key={i}>
                <img src={url} onLoad={handleOnLoad} alt="mag" />;
            </div>
        );
    });
    return (
        isReadNowPressed && (
            <div className="wrap h-full w-full flex justify-center items-center transform-none">
                <div id="mag-container" className="mag-container h-full w-full select-none ">
                    {imgs}
                </div>
            </div>
        )
    );
};

export default Mag;
