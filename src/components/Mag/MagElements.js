import styled from "styled-components";

export const MagContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    opacity: 0;
    background: white;
    position: absolute;
    top: 0;
    overflow: hidden;
    padding: 1.5rem 2.5rem 2.5rem;
`;

export const Magazine = styled.div`
    height: 100%;
    width: 100%;
    user-select: none;
    transition: margin-left 0.3s;

    img {
        width: 100%;
        height: 100%;
        cursor: pointer;
    }
`;

export const ActionsContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: max-content;
    position: absolute;
    top: 5%;
    right: 10%;
`;

export const IconContainer = styled.div`
    width: 1em;
    height: 1em;
    margin-right: 1em;

    @media screen and (min-width: 768px) {
        margin-right: 2em;
        width: 1.5em;
        height: 1.5em;
    }

    img {
        cursor: pointer;
        width: 100%;
        height: 100%;

        &:hover {
            opacity: 0.7;
        }
    }
`;
