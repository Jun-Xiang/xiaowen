import { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";
import Manrope from "./assets/fonts/manrope/Manrope-Regular.otf";
import ManropeExtraLight from "./assets/fonts/manrope/Manrope-ExtraLight.otf";
import ManropeLight from "./assets/fonts/manrope/Manrope-Light.otf";
import ManropeMedium from "./assets/fonts/manrope/Manrope-Medium.otf";
import ManropeSemiBold from "./assets/fonts/manrope/Manrope-SemiBold.otf";
import ManropeBold from "./assets/fonts/manrope/Manrope-Bold.otf";
import ManropeExtrabold from "./assets/fonts/manrope/Manrope-ExtraBold.otf";

const GlobalStyles = createGlobalStyle`
    ${normalize}
    @font-face {
        font-family: "Manrope";
        src: url(${Manrope});
    }

    @font-face {
        font-family: "Manrope ExtraLight";
        src: url(${ManropeExtraLight});
    }

    @font-face {
        font-family: "Manrope Light";
        src: url(${ManropeLight});
    }

    @font-face {
        font-family: "Manrope Medium";
        src: url(${ManropeMedium});
    }

    @font-face {
        font-family: "Manrope SemiBold";
        src: url(${ManropeSemiBold});
    }

    @font-face {
        font-family: "Manrope Bold";
        src: url(${ManropeBold});
    }

    @font-face {
        font-family: "Manrope Extrabold";
        src: url(${ManropeExtrabold});
    }
    :root {
        --fontM: "Manrope", sans-serif;
        --fontEl: "Manrope ExtraLight", sans-serif;
        --fontL: "Manrope Light", sans-serif;
        --fontMe: "Manrope Medium", sans-serif;
        --fontSb: "Manrope SemiBold", sans-serif;
        --fontBo: "Manrope Bold", sans-serif;
        --fontEb: "Manrope Extrabold", sans-serif;
    }

    *{
        box-sizing: border-box;
        margin: 0;
        padding: 0;
    }
    html,
    body,
    #root {
        width: 100%;
        height: 100%;
    }

    body{
        overflow: hidden;
        font-family: var(--fontM);
    }

    #root {
        font-size: 1rem;
        color: #40424F;
    }
    
    ul{
        list-style: none;
    }

    a{
        color: inherit;
        text-decoration: inherit;
    }
`;

export default GlobalStyles;
