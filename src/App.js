import React, { useState, useEffect, useLayoutEffect, createContext, useRef } from "react";
import GlobalStyles from "./GlobalStyles";
import Loading from "./components/Loading";
import Content from "./components/Content";
import Loader from "./components/Loader";
import Mag from "./components/Mag";
import Dialog from "./components/Dialog";

export const DialogContext = createContext();

const App = () => {
    const [status, setStatus] = useState("");
    const [selectedIssue, setSelectedIssue] = useState();
    const [isEn, setIsEn] = useState(true);
    const [dialogMsg, setDialogMsg] = useState({
        title: "",
        msg: "",
        btnText: "",
        btnClick: "",
        close: "",
    });
    const [showDialog, setShowDialog] = useState(false);
    const contentRef = useRef();
    // Kinda lazy and irrelevant to make landscape view in mobile :)

    useLayoutEffect(
        _ => {
            const windowResize = _ => {
                if (status === "READ") {
                    setStatus("READ_RESIZE");
                } else if (status === "DONE") {
                    setStatus("TRANSITION");
                    matchMedia("(orientation: landscape) and (max-width: 768px)").matches &&
                        alert(
                            "Please switch orientation to portrait for better experience.\n请将方向更改为纵向以获得更好的体验."
                        );
                }
            };
            window.addEventListener("resize", windowResize);
            return _ => window.removeEventListener("resize", windowResize);
        },
        [status]
    );

    useEffect(_ => setStatus("LOADING"), []);

    useEffect(
        _ => {
            if (
                status === "READ_LOADING" &&
                matchMedia("(orientation: landscape) and (max-width: 768px)").matches
            ) {
                alert("Please switch orientation to portrait to read.\n请将方向切换为纵向以阅读.");
                setStatus("LOADING");
            } else if (status === "READ_RESIZE") {
                setTimeout(_ => {
                    setStatus("READ");
                }, 1);
            }
        },
        [status]
    );
    const { title, msg, btnClick, btnText, close } = dialogMsg;

    return (
        <DialogContext.Provider value={{ setDialogMsg, setShowDialog }}>
            <GlobalStyles />
            {(status === "LOADING_TRANSITION" ||
                status === "LOADING" ||
                status === "CHANGE_YEAR") && (
                <Loading status={status} setStatus={setStatus} contentRef={contentRef} />
            )}
            <Content
                ref={contentRef}
                isEn={isEn}
                setIsEn={setIsEn}
                status={status}
                setStatus={setStatus}
                setSelectedIssue={setSelectedIssue}
            />
            {status === "READ_LOADING" && <Loader />}
            {showDialog && (
                <Dialog
                    additionalStyles="background: rgba(64, 66, 79, 0.2)"
                    title={title}
                    msg={msg}
                    btnText={btnText}
                    btnClick={btnClick}
                    close={close}
                />
            )}
            {selectedIssue &&
                !matchMedia("(orientation: landscape) and (max-width: 768px)").matches &&
                (status === "READ_LOADING" || status === "READ") && (
                    <Mag
                        selectedIssue={selectedIssue}
                        setStatus={setStatus}
                        status={status}
                        isEn={isEn}
                    />
                )}
        </DialogContext.Provider>
    );
};

export default App;
