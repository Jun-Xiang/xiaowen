# School Bulletin Website

### Disclaimer

-   This website is made by Chung Ling High School student and not professional. Please expect some errors and glitches. Any commits or issues are welcomed.

### What you can do to help

-   You can send your feedback to [here](https://forms.gle/eMVE1ankEiSEQdCt7)

-   You could also send any issue you find in the website to [here](https://forms.gle/6nyB3Rqv4Rvfdcy3A)

-   You could also create an issue or even better, pull request in gitlab for issues you find in this website.

### If you are a developer

-   If you would like to collaborate with me, please send an email to <jxooi0959@gmail.com>
